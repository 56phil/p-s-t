//
//  Painter_Sizer_TyperTests.m
//  Painter-Sizer-TyperTests
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//


//given

//when

//then

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import "OCHamcrest/OCHamcrest.h"

#import "OCMock/OCMock.h"

#define MOCKITO_SHORTHAND
#import "OCMockito/OCMockito.h"

#import "AppDelegate.h"
#import "TyperViewController.h"
#import "SizerViewController.h"
#import "PainterTableViewController.h"
#import "ColorDetailViewController.h"

@interface SizerViewController()
- (IBAction) minusPressed;
- (IBAction) plusPressed;
- (void) updateBadge;
@end

@interface TyperViewController()
- (void) updateTextColor;
- (void) updateFontSize;

-(void)touchesBegan:(NSSet *)touches
          withEvent:(UIEvent *)event;

- (BOOL)textFieldShouldReturn:(UITextField *)textField;

- (IBAction)typerTextFieldEditingDidEnd:(UITextField *)sender
                               forEvent:(UIEvent *)event;

- (BOOL) textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string;

@end

@interface PainterTableViewController()

@end

@interface ColorDetailViewController()

@end

@interface Painter_Sizer_TyperTests : XCTestCase
@end

@implementation Painter_Sizer_TyperTests
AppDelegate *appDelegateUnderTest;
TyperViewController *typerViewControllerUnderTest;
SizerViewController *sizerViewControllerUnderTest;
PainterTableViewController *painterViewControllerUndetTest;
ColorDetailViewController *colorDetailViewControllerUnderTest;

- (void)setUp {
    [super setUp];
    appDelegateUnderTest = [[AppDelegate alloc] init];
    sizerViewControllerUnderTest = [[SizerViewController alloc] init];
    sizerViewControllerUnderTest.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    typerViewControllerUnderTest = [[TyperViewController alloc] init];
    typerViewControllerUnderTest.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    painterViewControllerUndetTest = [[PainterTableViewController alloc] init];
    colorDetailViewControllerUnderTest = [[ColorDetailViewController alloc] init];
}

- (void)tearDown {
    [super tearDown];
    appDelegateUnderTest = nil;
    typerViewControllerUnderTest = nil;
    sizerViewControllerUnderTest = nil;
    painterViewControllerUndetTest = nil;
    colorDetailViewControllerUnderTest = nil;
}

-(void) testMinusPressedStopsAtZero {
    //given
    sizerViewControllerUnderTest.appDelegate.size = 0;
    unsigned expectedResult = 0;
    
    //when
    [sizerViewControllerUnderTest minusPressed];
    unsigned result = sizerViewControllerUnderTest.appDelegate.size;
    
    //then
    assertThatUnsignedInt(result, is(equalToUnsignedInt(expectedResult)));
}

-(void) testMinusPressedDecrements {
    //given
    sizerViewControllerUnderTest.appDelegate.size = 10;
    unsigned expectedResult = 9;
    
    //when
    [sizerViewControllerUnderTest minusPressed];
    unsigned result = sizerViewControllerUnderTest.appDelegate.size;
    
    //then
    assertThatUnsignedInt(result, is(equalToUnsignedInt(expectedResult)));
}

-(void) testPlusPressedStopsAtTwenty {
    //given
    unsigned expectedResult = 20;
    sizerViewControllerUnderTest.appDelegate.size = 20;
    
    //when
    [sizerViewControllerUnderTest plusPressed];
    unsigned result = sizerViewControllerUnderTest.appDelegate.size;
    
    //then
    assertThatUnsignedInt(result, is(equalToUnsignedInt(expectedResult)));
}

-(void) testPlusPressedIncrements {
    //given
    unsigned expectedResult = 10;
    sizerViewControllerUnderTest.appDelegate.size = 9;
    
    //when
    [sizerViewControllerUnderTest plusPressed];
    unsigned result = sizerViewControllerUnderTest.appDelegate.size;
    
    //then
    assertThatUnsignedInt(result, is(equalToUnsignedInt(expectedResult)));
}

-(void) testUpdateBadge {
    //given
    sizerViewControllerUnderTest.appDelegate.size = 0;
    NSString *expectedResult = @"0";
    
    //when
    [sizerViewControllerUnderTest updateBadge];
    NSString *result = sizerViewControllerUnderTest.tabBarItem.badgeValue;
    
    //then
    assertThat(result, is(equalTo(expectedResult)));
}

-(void) testUpdateFontSizeUpdatesFontName {
    //given
    id mockNavController = [OCMockObject mockForClass:[UINavigationController class]];
    TyperViewController *sut = [[TyperViewController alloc] init];
    id typerViewControllerMock = [OCMockObject partialMockForObject:sut];
    [[[typerViewControllerMock stub] andReturn:mockNavController] navigationController];
    [[typerViewControllerMock expect] updateFontSize];
    
    //when
    [sut updateFontSize];
    
    //then
    [mockNavController verify];
    [typerViewControllerMock verify];
}

-(void) testUpdateTextColorUpdatesTextColor {
    //given
    id mockNavController = [OCMockObject mockForClass:[UINavigationController class]];
    TyperViewController *sut = [[TyperViewController alloc] init];
    id typerViewControllerMock = [OCMockObject partialMockForObject:sut];
    [[[typerViewControllerMock stub] andReturn:mockNavController] navigationController];
    [[typerViewControllerMock expect] updateTextColor];
    
    //when
    [sut updateTextColor];
    
    //then
    [mockNavController verify];
    [typerViewControllerMock verify];
}

-(void) testUpdateTextColor {
    //given
    typerViewControllerUnderTest.appDelegate.theColor = [UIColor brownColor];
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    [[mockTextField expect] setTextColor:typerViewControllerUnderTest.appDelegate.theColor];
    
    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    [typerViewControllerUnderTest updateTextColor];
    
    //then
    [mockTextField verify];
}

-(void) testUpdateFontSize {
    //given
    typerViewControllerUnderTest.appDelegate.size = 12;
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    [[mockTextField expect] setFont:[UIFont fontWithName:@"ArialMT"
                                                    size:12.0]];
    [[mockTextField expect] setAdjustsFontSizeToFitWidth:NO];
    
    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    [typerViewControllerUnderTest updateFontSize];
    
    //then
    [mockTextField verify];
}

-(void) testTouchesBegan {
    //given
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    [[mockTextField expect] resignFirstResponder];
    
    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    [typerViewControllerUnderTest touchesBegan:nil withEvent:nil];
    
    //then
    [mockTextField verify];
}

- (void)testTextFieldShouldReturn {
    //given
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    [[mockTextField expect] resignFirstResponder];
    
    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    [typerViewControllerUnderTest textFieldShouldReturn:typerViewControllerUnderTest.typerTextField];
    
    //then
    [mockTextField verify];
}

-(void)testTextFieldshouldChangeCharactersInRangeReplacementString {
    //given
    typerViewControllerUnderTest.appDelegate.size = 12;
    typerViewControllerUnderTest.appDelegate.theColor = [UIColor blueColor];
    
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    OCMStub([mockTextField font])
    .andReturn([UIFont fontWithName:@"ArialMT" size:12.0]);
    [[mockTextField expect] setFont:[UIFont fontWithName:@"ArialMT" size:12.0]];
    [[mockTextField expect] setAdjustsFontSizeToFitWidth:NO];
    [[mockTextField expect] setTextColor:typerViewControllerUnderTest.appDelegate.theColor];
    [[mockTextField expect] text];
    [[mockTextField expect] frame];
    
    id mockInputString = [OCMockObject mockForClass:[NSString class]];
    mockInputString = @"iv";
    
    id mockTopLabel = [OCMockObject mockForClass:[UILabel class]];
    [[mockTopLabel expect] setFont:[UIFont fontWithName:@"ArialMT" size:12.0]];
    [[mockTopLabel expect] setAdjustsFontSizeToFitWidth:NO];

    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    typerViewControllerUnderTest.typerTopLabel = mockTopLabel;
    [typerViewControllerUnderTest textField:typerViewControllerUnderTest.typerTextField shouldChangeCharactersInRange:NSRangeFromString(mockInputString)
   replacementString:mockInputString];
    
    //then
    [mockTextField verify];
    [mockTopLabel verify];
}

- (void) testTyperTextFieldEditingDidEnd {
    //given
    id mockTextField = [OCMockObject mockForClass:[UITextField class]];
    [[mockTextField expect] resignFirstResponder];
    
    //when
    typerViewControllerUnderTest.typerTextField = mockTextField;
    [typerViewControllerUnderTest typerTextFieldEditingDidEnd:mockTextField forEvent:nil];
    
    //then
    [mockTextField verify];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
