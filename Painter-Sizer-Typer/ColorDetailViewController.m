//
//  ColorViewController.m
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-07.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import "ColorDetailViewController.h"
#import "AppDelegate.h"

@interface ColorDetailViewController ()

@end

@implementation ColorDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)displayBackgroundColor:(id)sender {
    self.view.backgroundColor = self.passedColor;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
