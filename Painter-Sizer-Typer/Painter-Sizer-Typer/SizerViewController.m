//
//  SizerViewController.m
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
// 

#import "SizerViewController.h"

@interface SizerViewController ()

@end

@implementation SizerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self updateBadge];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)minusPressed {
    self.appDelegate.size -= self.appDelegate.size > FONT_SIZE_MIN ? 1 : 0;
    [self updateBadge];
}

- (IBAction)plusPressed {
    self.appDelegate.size += self.appDelegate.size < FONT_SIZE_MAX ? 1 : 0;
    [self updateBadge];
}

-(void) updateBadge {
    self.tabBarItem.badgeValue = [self.appDelegate.numberFormatter stringFromNumber:[[NSNumber alloc] initWithUnsignedInt:self.appDelegate.size]];
}

@end
