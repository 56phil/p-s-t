//
//  ColorItem.h
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-05.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ColorItem : NSObject
@property (nonatomic, readonly, strong) NSString *colorName;
@property (nonatomic, readonly, strong) UIColor *theColor;
@property (nonatomic, readonly, strong) UIColor *assignedTextColor;

-(id)initWithColorName :(NSString*)colorName
           andTheColor :(UIColor*)theColor
  andAssignedTextColor :(UIColor*)assignedTextColor;
@end
