//
//  PainterTableViewController.h
//  Painter-Sizer-Typer 
//
//  Created by Philip Huffman on 2015-06-05.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PainterTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *colorArray;
@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic, strong) NSString *myReuseIdentifier;
@property (nonatomic, strong) AppDelegate *appDelegate;
@end
