//
//  main.m
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
