//
//  TyperViewController.m
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

//todo manage kb

#import "TyperViewController.h"

@interface TyperViewController ()

@end

@implementation TyperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self updateFontSize];
    [self updateTextColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateTextColor {
    self.typerTextField.textColor = self.appDelegate.theColor;
}

-(void) updateFontSize {
    CGFloat requestedPointSize = self.appDelegate.size > 0 ? self.appDelegate.size : 0.001;
    self.typerTopLabel.font = [UIFont fontWithName:@"ArialMT"
                                              size:requestedPointSize];
    self.typerTopLabel.adjustsFontSizeToFitWidth = NO;
    
    self.typerTextField.font = [UIFont fontWithName:@"ArialMT"
                                               size:requestedPointSize];
    self.typerTextField.adjustsFontSizeToFitWidth = NO;
}

-(BOOL) textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    if (textField == self.typerTextField) {
        [self updateTextColor];
        [self updateFontSize];
        NSLog(@"%@", textField.font.fontName);
        NSDictionary *attributes = @{NSFontAttributeName:self.typerTextField.font};
        if ([self getAvailableWidthInTextField:textField usingAdjustmentFactor:0.96 withAttributes:attributes]
            <
            [self getRequiredWidthForString:string withAttributes:attributes]) {
            return NO;
        }
        self.typerTopLabel.text = [textField.text stringByAppendingString:string];
    }
    return YES;
}

- (CGFloat) getAvailableWidthInTextField:(UITextField*)textField
                   usingAdjustmentFactor:(CGFloat)adjustmentFactor
                          withAttributes:(NSDictionary*)attributes {
    CGSize sizeUsed = [textField.text sizeWithAttributes:attributes];
    return textField.frame.size.width * adjustmentFactor - sizeUsed.width;
}

- (CGFloat) getRequiredWidthForString:(NSString*)string
                       withAttributes:(NSDictionary*)attributes {
    CGSize stringSize = [string sizeWithAttributes:attributes];
    return stringSize.width;
}

- (IBAction)typerTextFieldEditingDidEnd:(UITextField *)sender
                               forEvent:(UIEvent *)event {
    [self.typerTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches
          withEvent:(UIEvent *)event {
    [self.typerTextField resignFirstResponder];
}

@end
