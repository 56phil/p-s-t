//
//  TyperViewController.h
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TyperViewController : UIViewController <UITextFieldDelegate> 
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, weak) IBOutlet UILabel *typerTopLabel;
@property (nonatomic, weak) IBOutlet UITextField *typerTextField;
@end

