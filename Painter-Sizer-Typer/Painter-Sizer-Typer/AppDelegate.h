//
//  AppDelegate.h
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorItem.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIColor *theColor;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
@property (nonatomic) unsigned size;
@end

