//
//  ColorItem.m
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-05.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import "ColorItem.h"

@implementation ColorItem
-(id)initWithColorName :(NSString*)colorName
           andTheColor :(UIColor*)theColor
  andAssignedTextColor :(UIColor *)assignedTextColor {
    if (self = [super init]) {
        _colorName = colorName;
        _theColor = theColor;
        _assignedTextColor = assignedTextColor;
        return self;
    }
    return nil;
}

@end
