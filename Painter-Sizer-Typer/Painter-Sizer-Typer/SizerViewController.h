//
//  SizerViewController.h
//  Painter-Sizer-Typer 
//
//  Created by Philip Huffman on 2015-06-04.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

static const unsigned FONT_SIZE_MAX = 20;
static const unsigned FONT_SIZE_MIN = 0;

@interface SizerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *plusMinusContainer;
@property (strong, nonatomic) AppDelegate *appDelegate;
@end
