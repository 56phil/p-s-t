//
//  ColorViewController.h
//  Painter-Sizer-Typer
//
//  Created by Philip Huffman on 2015-06-07.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ColorDetailViewController : UIViewController

@property (nonatomic, strong) UIColor *passedColor;

@end
